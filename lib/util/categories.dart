import 'package:font_awesome_flutter/font_awesome_flutter.dart';

List categories = [
  {
    "name": "Bebidas",
    "icon": FontAwesomeIcons.wineBottle,
    "items": 5
  },
  {
    "name": "Vegetariana",
    "icon": FontAwesomeIcons.cannabis,
    "items": 20
  },
  {
    "name": "Postres",
    "icon": FontAwesomeIcons.birthdayCake,
    "items": 9
  },
  {
    "name": "Comida rapida",
    "icon": FontAwesomeIcons.pizzaSlice,
    "items": 5
  },
  {
    "name": "Carnes",
    "icon": FontAwesomeIcons.breadSlice,
    "items": 15
  },
];