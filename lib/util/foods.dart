List<Map> foods = [
  {
    "img": "assets/food1.jpeg",
    "name": "Ensalada de frutas"
  },
  {
    "img": "assets/food2.jpeg",
    "name": "Ensalada de frutas"
  },
  {
    "img": "assets/food3.jpeg",
    "name": "Hamburguesa"
  },
  {
    "img": "assets/food4.jpeg",
    "name": "Ensalada de frutas"
  },
  {
    "img": "assets/food5.jpeg",
    "name": "Hamburguesa"
  },
  {
    "img": "assets/food6.jpeg",
    "name": "Filete"
  },
  {
    "img": "assets/food7.jpeg",
    "name": "Pizza"
  },
  {
    "img": "assets/food8.jpeg",
    "name": "Espárragos"
  },
  {
    "img": "assets/food9.jpeg",
    "name": "Ensalada"
  },
  {
    "img": "assets/food10.jpeg",
    "name": "Pizza"
  },
  {
    "img": "assets/food11.jpeg",
    "name": "Pizza"
  },
  {
    "img": "assets/food12.jpg",
    "name": "Ensalada"
  },
];